This project is a very simple end-to-end test for DAST. 

The project runs a DAST scan against a WebGoat Docker image, makes very minor changes to the file, and then compares the results it has against an expectation file. 